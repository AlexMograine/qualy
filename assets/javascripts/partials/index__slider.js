$(function(){

    $('.index__carousel').owlCarousel({
        loop: true,
        margin: 10,
        items: 1,
        dots: true,
        nav: false
    });

});
