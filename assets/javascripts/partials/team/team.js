$(function(){

    $('.members__items .members__item:first-child').addClass('members__item_disable');

    $('.members .members__item').click(function(){
            
        var itemName = $(this).find('.members__item_inner--name').text().trim();
        var itemPosition = $(this).find('.members__item_inner--position').text().trim();
        var itemDesc = $(this).find('.members__item_inner--desc').text().trim();
        var itemImage = $(this).find('.members__item_inner--img img').attr('src');
            
        $(this).parent().parent().find('.members__item').removeClass('members__item_disable')
        $(this).addClass('members__item_disable')

        $(this).parent().parent().find('.member__active .member__active_desc--name').text(itemName);
        $(this).parent().parent().find('.member__active .member__active_desc--position').text(itemPosition);
        $(this).parent().parent().find('.member__active .member__active_desc--desc').text(itemDesc);
        $(this).parent().parent().find('.member__active .member__active_img--src img').attr('src', itemImage);
  
        $([document.documentElement, document.body]).animate({
            scrollTop: $(this).parent().parent().find('.member__active').offset().top-25
        }, 1000);
        
    })

})