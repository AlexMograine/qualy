$(function () {
    $('.burger').click(function() {
        $('.burger').toggleClass('cross');
        $('.header__nav_mobile').toggleClass('header__nav_mobile--active');
    });

    $('.nav__item').on('click', function(event){
        event.stopPropagation();

        $('.nav__item').removeClass('nav__item--active');
        $('.nav__item').toggleClass('disable');
        $(this).toggleClass('disable');

        $('.nav__item').each(function() {
            if ($(this).hasClass('nav__item--active')) {
                $(this).removeClass('nav__item--active');
            }
        });

        $(this).toggleClass('nav__item--active');
        $(this).next('.nav__container_mobile_menu--wrapper').children().toggleClass('active');
    });
});
