$(function(){

    $('.product__slider_big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product__slider_small'
    });
    $('.product__slider_small').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.product__slider_big',
        dots: false,
        focusOnSelect: true
    });
    

    $('.product__about_slider').owlCarousel({
        loop: true,
        margin: 10,
        items: 1,
        dots: true,
        nav: true
    })

})