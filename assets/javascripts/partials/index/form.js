$(document).ready(function () {

    $(".index__form_inner").validate({

        rules: {
            name: { required: true },
            lastname: { required: true },
            phone: { required: true },
            email: { required: true, email: true },
        },
        messages: {
            name: '',
            lastname: '',
            phone: '',
            email: '',
        },

        submitHandler: function (form, e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: '/wp-content/themes/qualysense/forms/main-form.php',
                dataType: "html",
                data: {
                    name: $('.recall__form_textarea').val(),
                    lastname: $('.recall__form_textarea').val(),
                    phone: $('.recall__form_textarea').val(),
                    email: $('.recall__form_textarea').val(),
                    textarea: $('.recall__form_textarea').val()
                },
                success: function (result) {
                    console.log('success')
                },
                error: function (error) {
                    console.log('error');
                }
            });
            return false;
        }
    });

    $('.input__container_phone').inputmask("(999) 999-9999");

})
